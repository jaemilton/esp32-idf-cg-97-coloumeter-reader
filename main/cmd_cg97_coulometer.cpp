/* UART asynchronous example, that uses separate RX and TX tasks

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "cmd_cg97_coulometer.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_log.h"
#include "string.h"
#include <math.h>
#include "cmd_mqtt_client.h"
//#include <stdio.h>



static const char *TAG_CG97 = "CG97";
//#define CG97_COULOMETER_BAUD_RATE 19200
//#define TXD_PIN (GPIO_NUM_17)
//#define RXD_PIN (GPIO_NUM_16)
//#define UART_READ_TIMEOUT 100
#define DELAY_MEANSURE_CG97_MILLISECOND 5000


string get_meansure_mqtt_message(const power_meansuare_t* mensures)
{
	string message_mqtt;
	std::stringstream messagestream;

	stringstream  read_time;   // stream used for the conversion
	read_time << mensures->read_time;      // insert the textual representation of 'Number' in the characters in the stream
	messagestream << read_time.str() << ";";

	messagestream << std::to_string(mensures->temperature)  << ";";

	stringstream  battery_percentage;   // stream used for the conversion
	battery_percentage << mensures->battery_percentage;      // insert the textual representation of 'Number' in the characters in the stream
	messagestream << battery_percentage.str() << ";";
	messagestream << std::to_string(mensures->voltage)  << ";";
	messagestream << std::to_string(mensures->current)  << ";";
	messagestream << std::to_string(mensures->power) << ";";
	messagestream << std::to_string(mensures->eletrical_value) << ";";
	messagestream >> message_mqtt;
	return message_mqtt;
}





void print_meansures(const task_data_t * task_data, const power_meansuare_t * mensures)
{

	float voltage = mensures->voltage;
	if( !isnan(voltage) ){
		ESP_LOGI(TAG_CG97, "%s - Voltage: %.4f V", task_data->task_name, voltage);
	} else {
		ESP_LOGI(TAG_CG97, "%s - Error reading voltage", task_data->task_name);
	}

	float current = mensures->current;
	if( !isnan(current) ){
		ESP_LOGI(TAG_CG97, "%s - Current: %.4f A", task_data->task_name, current);
	} else {
		ESP_LOGI(TAG_CG97, "%s - Error reading current", task_data->task_name);
	}

	float power = mensures->power;
	if( !isnan(power) ){
		ESP_LOGI(TAG_CG97, "%s - Power: %.6f W", task_data->task_name, power );
	} else {
		ESP_LOGI(TAG_CG97, "%s - Error reading power",task_data->task_name);
	}

	u_int8_t battery_percentage = mensures->battery_percentage;
	if( !isnan(battery_percentage) ){
		ESP_LOGI(TAG_CG97, "%s - Battery percentage: %d percent",task_data->task_name, battery_percentage);
	} else {
		ESP_LOGI(TAG_CG97, "%s - Error reading battery percentage",task_data->task_name);
	}

	float eletrical_value = mensures->eletrical_value;
	if( !isnan(eletrical_value) ){
		ESP_LOGI(TAG_CG97, "%s - Electrical Value: %.1f AH", task_data->task_name, eletrical_value);
	} else {
		ESP_LOGI(TAG_CG97, "%s - Error reading electrical Value", task_data->task_name);
	}

	float temperature = mensures->temperature;
	if( !isnan(temperature) ){
		ESP_LOGI(TAG_CG97, "%s - Temperature: %.2f C",task_data->task_name, temperature);
	} else {
		ESP_LOGI(TAG_CG97, "%s - Error reading Temperature",task_data->task_name);
	}

}

void meansure_cg97_task(void *arg)
{
	const task_data_t * task_data = (const task_data_t *) arg;
		uart_data_t uart_data = task_data->uart_data;

	ESP_LOGI(TAG_CG97, "Initializing Task %s on Core %i using UART %i", task_data->task_name,  xPortGetCoreID(),  task_data->uart_data.uart_port);
	ESP_LOGI(TAG_CG97, "TopicName = %s", task_data->mqtt_topic_name);

	/* Use software serial for the PZEM*/
	CG97COULOMETER cg97(&uart_data);

	while (1) {

		//Ensure that mqtt is connected
		if(esp_wait_for_mqtt_connection(get_tick_type_for_milliseconds(1000)))
		{
			power_meansuare_t * mensures = cg97.meansures();
			if (mensures != NULL)
			{
				//ESP_LOGI(TAG_CG97, "meansures read.");
				//print_meansures(task_data, mensures);
				//ESP_LOGI(TAG, "Read %d bytes.", len);
				//ESP_LOG_BUFFER_HEXDUMP(TAG, resp, len, ESP_LOG_INFO);
				//ESP_LOGI(TAG_CG97, "Sending meansures to mqtt topic %s", task_data->mqtt_topic_name);
				string meansuresMqttMessage = get_meansure_mqtt_message(mensures);
				//ESP_LOGI(TAG_CG97, "%s >> %s", task_data->task_name, meansuresMqttMessage.c_str());
				mqtt_send_message(task_data->mqtt_topic_name, meansuresMqttMessage.c_str());
				//print_meansures(task_data, mensures);
			}
			vTaskDelay( get_tick_type_for_milliseconds(DELAY_MEANSURE_CG97_MILLISECOND)  );
		}
		else
		{
			taskYIELD();
		}
	}
}


void start_task_uart(const task_data_t * task_data)
{

	uart_data_t uart_data = task_data->uart_data;
	TaskHandle_t  xHandle = NULL;
	ESP_LOGI(TAG_CG97, "Task variables %s, UART: %d", task_data->task_name, uart_data.uart_port);
	xTaskCreate(meansure_cg97_task, task_data->task_name, STACK_SIZE, (void *) task_data, 10, &xHandle);
	configASSERT( xHandle );

//	//stores the task handle to interact with then when necessary
//	TaskHandle[_contator_task++] = xHandle;

	 // Use the handle to delete the task.
	if( xHandle != NULL )
	{
		ESP_LOGI(TAG_CG97, "Task %s initialized", task_data->task_name);
	}




//	esp_log_level_set(TAG_CG97_TX, ESP_LOG_VERBOSE);
//	ESP_LOGI(TAG_CG97_TX, "cg97 TX task started.");
//	bool reading_data_started = false;
//
//	/* Use software serial for the PZEM*/
//	CG97COULOMETER pzem(&uart_data);
//
//    while (1)
//    {
//		if (esp_wait_for_mqtt_connection(get_tick_type_for_milliseconds(1000)))
//		{
//			if (!reading_data_started)
//			{
//
//				// 4. To issue mandatory instructions to the equipment:
//				//Practical example:     77 33 8A
//				// Number correspondence: 01 02 03
//				// 01 02: Identification Header 03: Instruction Code [8A-Unconditional Stop Output 8B-Unconditional Single Output Set of Data
//				ESP_LOGI(TAG_CG97_TX, "Sending 0x77338A.");
//				u_int8_t unconditional_stop[3] = { 0x77, 0x33, 0x8A } ;
//				sendData(TAG_CG97_TX, unconditional_stop, 3);
//				//ESP_LOGI(TAG_CG97_TX, "Sent 0x77338A.");
//				ESP_LOGI(TAG_CG97_TX, "Sending 0x7733C142.");
//				char start_continous_output_data [4] ={ 0x77, 0x33, 0xC1, 0x42 };
//				sendData(TAG_CG97_TX, start_continous_output_data, 4); //ask for device address C1 for Start Continuous Output Data
//				//ESP_LOGI(TAG_CG97_TX, "Sent 0x7733C142.");
//				reading_data_started = true;
//			}
//			vTaskDelay( get_tick_type_for_milliseconds(1000));
//		}
//		else
//		{
//			ESP_LOGI(TAG_CG97_TX, "Waiting MQTT connection.");
//			if (reading_data_started)
//			{
//				//2. Send instructions to the device to change the data output mode
//				//Practical example:     77 33 C0 41
//				//Number correspondence: 01 02 03 04
//				//01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [40 - Stop Output 41 - Output a set of data 42 - Start Continuous Output Data] Outgoing by default 40
//				ESP_LOGI(TAG_CG97_TX, "Sending 0x7733C140.");
//				char stop_continous_output_data[4] = { 0x77,0x33,0xC1, 0x40 } ;
//				sendData(TAG_CG97_TX, stop_continous_output_data, 4); //ask for device address C1 to Stop Output
//				reading_data_started = false;
//			}
//			vTaskDelay( get_tick_type_for_milliseconds(2000));
//
//		}
//
//		taskYIELD();
//
//    }
//    ESP_LOGI(TAG_CG97_TX, "cg97 TX task stopped.");
}







//
//static void rx_task(void *arg)
//{
//	esp_log_level_set(TAG_CG97_RX, ESP_LOG_VERBOSE);
//	ESP_LOGI(TAG_CG97_RX, "cg97 RX task started.");
//	//uint8_t* data = (uint8_t*) malloc(RX_BUF_SIZE+1);
//
//	uint16_t rxBytes = 31;
//	uint8_t* resp = (uint8_t*) malloc(rxBytes);
//	uint8_t* c = (uint8_t*) malloc(1);
//
//	while (1)
//	{
//
//		ESP_LOGI(TAG_CG97_RX, "cg97 start reading data.");
//		uint8_t index = 0; // Bytes we have read
//		if (esp_wait_for_mqtt_connection(get_tick_type_for_milliseconds(1000)))
//		{
//			while(index < rxBytes)
//			{
//
//				int length = uart_read_bytes(UART_NUM_2, c, 1, UART_READ_TIMEOUT / portTICK_RATE_MS);
//
//				if (length > 0)
//				{
//					resp[index++] = *c;
//				}
//				//taskYIELD();
//			}
//
//
//			ESP_LOGI(TAG_CG97_RX, "Read %d bytes: '%s'", rxBytes, resp);
//			ESP_LOG_BUFFER_HEXDUMP(TAG_CG97_RX, resp, rxBytes, ESP_LOG_INFO);
//
////			const int rxBytes = uart_read_bytes(UART_NUM_2, data, RX_BUF_SIZE, 100 / portTICK_RATE_MS);
////			if (rxBytes > 0) {
////				data[rxBytes] = 0;
////				ESP_LOGI(TAG_CG97_RX, "Read %d bytes: '%s'", rxBytes, data);
////				ESP_LOG_BUFFER_HEXDUMP(TAG_CG97_RX, data, rxBytes, ESP_LOG_INFO);
////				//string meansuresMqttMessage = get_meansure_mqtt_message(task_data, mensures);
////				//mqtt_send_message("cg97/meansure", rxBytes, (const char*)data);
////			}
//		}
//		else
//		{
//			ESP_LOGI(TAG_CG97_RX, "Waiting MQTT connection.");
//			vTaskDelay( get_tick_type_for_milliseconds(2000));
//		}
//		//free(resp);
//		//taskYIELD();	// do background netw tasks while blocked for IO (prevents ESP watchdog trigger)
//		vTaskDelay( get_tick_type_for_milliseconds(1));
//
//
//	}
//	ESP_LOGI(TAG_CG97_RX, "cg97 RX task stopped.");
//}

void cg97_start(void)
{

	static uart_data_t uart = {
			.uart_port = CONFIG_UART_NUM,
			.tx_io_num = CONFIG_UART_TX_PIN,
			.rx_io_num = CONFIG_UART_RX_PIN
		};

	static task_data_t task_data = {
		.task_name =	"READING_CG97",
		.mqtt_topic_name = CONFIG_TOPIC_NAME,
		.uart_data = uart
	  };
	  start_task_uart(&task_data);

}
