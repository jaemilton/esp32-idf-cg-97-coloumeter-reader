/*
 * CG97COULOMETER.h
 *
 *  Created on: 1 de out de 2021
 *      Author: jaemi
 */

#ifndef MAIN_INCLUDE_CG97COULOMETER_H_
#define MAIN_INCLUDE_CG97COULOMETER_H_
#include "string.h"
#include <stdint.h>
#include "driver/uart.h"

typedef struct
{
	uart_port_t uart_port;
	int tx_io_num;
	int rx_io_num;
} uart_data_t;


typedef struct
{
	char *task_name;
	char *mqtt_topic_name;
	uart_data_t uart_data;
} task_data_t;




typedef struct {
         float voltage;
         float current;
         float power;
         float eletrical_value;
         unsigned int read_time;
         float temperature;
         unsigned int battery_percentage;
     }  power_meansuare_t; // Measured values

#define CG97_DEFAULT_ADDR    0xC1
#define CG97_BAUD_RATE 19200
#define MESSAGE_LENGTH 31
#define CMD_IDENTIFICATION_HEADER_00 0x77
#define CMD_IDENTIFICATION_HEADER_01 0x33
#define CMD_STOP_OUTPUT 0x40
#define CMD_OUTPUT_A_SET_OF_DATA 0x41
#define CMD_START_CONTINOUS_OUTPUT_DATA 0x42
#define CMD_ISSUE_ZERO_CLEANING_POWER 0x03
#define CMD_UNCONDITIONAL_STOP_OUTPUT 0x8A
#define CMD_UNCONDITIONAL_SINGLE_OUTPUT_SET_OF_DATA 0x8B

class CG97COULOMETER {
public:

    CG97COULOMETER(uart_data_t *uart_data, uint8_t addr=CG97_DEFAULT_ADDR);
	virtual ~CG97COULOMETER();



	power_meansuare_t* meansures();
	float voltage();
	float current();
	float power();
	float eletrical_value();
	int read_time();
	float temperature();
	float battery_percentage();

	// 1. To issue zero-clearing power instructions to equipment
	// Practical example:     77 33 C0 03
	// Number correspondence: 01 02 03 04
	// 01 02: Identification Header 03: Device Address + 0xC4: This Record Zero Clearing Instruction Code 0x03
	void issue_zero_clearing_power();

	// 2. Send instructions to the device to change the data output mode
	// Practical example:     77 33 C0 41
	// Number correspondence: 01 02 03 04
	// 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [40 - Stop Output 41 - Output a set of data 42 - Start Continuous Output Data] Outgoing by default 40
	void change_output_mode(uint8_t mode=CMD_STOP_OUTPUT);

	// 3. Issue instructions to fix the address of the equipment:
	// Practical example:     77 33 C0 81
	// Number correspondence: 01 02 03 04
	// 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [New Address = 0x81-0x80] Address Range 0x00-0x7f Total 128 So Instruction Code Range is 0x80-0xff
	void fix_address(uint8_t newAddress);

	// 4. To issue mandatory instructions to the equipment:
	// Practical example:     77 33 8A
	// Number correspondence: 01 02 03
	// 01 02: Identification Header 03: Instruction Code [8A-Unconditional Stop Output 8B-Unconditional Single Output Set of Data
	// This command ignores the device address and can be used to view the device address when it is forgotten that the device address cannot be controlled.
	void set_mandatoty_instructions(uint8_t instructionCode=CMD_UNCONDITIONAL_STOP_OUTPUT);

	private:

	    bool _isSoft;    // Is serial interface software

	    uint8_t _addr;   // Device address
	    uint64_t _lastRead; // Last time values were updated
	    power_meansuare_t _currentValues;

	    void init(uint8_t addr); // Init common to all constructors

	    bool updateValues();    // Get most up to date values from device registers and cache them
	    uint16_t recieve(uint8_t *resp, uint16_t len); // Receive len bytes into a buffer

	    int sendData(uint8_t cmd);

	    bool checkCRC(const uint8_t *buf, uint16_t len);   // Check CRC of buffer

	    uart_data_t * _uart_data;

	    char* copyUint8Array(uint8_t * array);

	    uint64_t millis();

};

#endif /* MAIN_INCLUDE_CG97COULOMETER_H_ */
