/*
 * CG97COULOMETER.cpp
 *
 *  Created on: 1 de out de 2021
 *      Author: jaemi
 */

#include "CG97COULOMETER.h"

#include <stdio.h>
#include "sdkconfig.h"
#include "esp_log.h"
#include <math.h>
#include "driver/gpio.h"
#include "driver/uart.h"
#include <inttypes.h>

#define TAG "CG97COULOMETER"


#define UPDATE_TIME_MILLISECOUNDS    1000
#define READ_TIMEOUT 1000
#define RX_BUF_SIZE 512




CG97COULOMETER::CG97COULOMETER(uart_data_t *uart_data, uint8_t addr)
{

	this->_uart_data = uart_data;

	 const uart_config_t uart_config = {
	        .baud_rate = CG97_BAUD_RATE,
	        .data_bits = UART_DATA_8_BITS,
	        .parity = UART_PARITY_DISABLE,
	        .stop_bits = UART_STOP_BITS_1,
	        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
	        .source_clk = UART_SCLK_APB,

	    };

    //	 We won't use a buffer for sending data.
	// Initialize WART
	ESP_ERROR_CHECK(uart_driver_install(this->_uart_data->uart_port, RX_BUF_SIZE * 2, 0, 0, NULL, 0));
	ESP_ERROR_CHECK(uart_param_config(this->_uart_data->uart_port, &uart_config));
	ESP_ERROR_CHECK(uart_set_pin(this->_uart_data->uart_port, uart_data->tx_io_num, uart_data->rx_io_num, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE));


	init(addr);

}




CG97COULOMETER::~CG97COULOMETER() {
	// TODO Auto-generated destructor stub
}



/*!
 * CG97COULOMETER::meansures
 *
 * Get all line meansures
 *
 * @return current L-N
 * 		Volage in Volts
 * 		Current in Amps
 * 		Active power in W
 * 		Active energy in kWh since last reset
 * 		Frequency in Hz
 * 		Power factor of load
 * 		Alarm triggerd
*/
power_meansuare_t * CG97COULOMETER::meansures()
{
   if(!updateValues()) // Update vales if necessary
		return NULL; // Update did not work, return NAN
	return &_currentValues;
}

/*!
 * CG97COULOMETER::voltage
 *
 * Get line voltage in Volts
 *
 * @return line L-N volage
*/
float CG97COULOMETER::voltage()
{
    if(!updateValues()) // Update vales if necessary
        return NAN; // Update did not work, return NAN

    return _currentValues.voltage;
}

/*!
 * CG97COULOMETER::current
 *
 * Get line in Amps
 *
 * @return line current
*/
float CG97COULOMETER::current()
{
    if(!updateValues())// Update vales if necessary
        return NAN; // Update did not work, return NAN

    return _currentValues.current;
}

/*!
 * CG97COULOMETER::power
 *
 * Get Active power in W
 *
 * @return active power in W
*/
float CG97COULOMETER::power()
{
    if(!updateValues()) // Update vales if necessary
        return NAN; // Update did not work, return NAN

    return _currentValues.power;
}

/*!
 * CG97COULOMETER::eletrical_value
 *
 * Get Active eletrical_value in AH (Amper/Hour)
 *
 * @return active eletrical_value in AH
*/
float CG97COULOMETER::eletrical_value()
{
    if(!updateValues()) // Update vales if necessary
        return NAN; // Update did not work, return NAN

    return _currentValues.eletrical_value;
}

/*!
 * CG97COULOMETER::read_time
 *
 * Get read_time in min since last reset
 *
 * @return line read_time in min
*/
int CG97COULOMETER::read_time()
{
    if(!updateValues()) // Update vales if necessary
        return NAN; // Update did not work, return NAN

    return _currentValues.read_time;
}

/*!
 * CG97COULOMETER::temperature
 *
 * Get current temperature in Ceusius
 *
 * @return temperature on C
*/
float CG97COULOMETER::temperature()
{
    if(!updateValues()) // Update vales if necessary
        return NAN; // Update did not work, return NAN

    return _currentValues.temperature;
}


/*!
 * CG97COULOMETER::init
 *
 * Set device address
 * Range 0x00-0x7f Total 128 So Instruction Code Range is 0x80-0xff
 *
 * @param[in] addr - device address
 *
 * @return success
*/
void CG97COULOMETER::init(uint8_t addr){
    if(addr < 0x01 || addr > 0x7f) // Sanity check of address
        addr = CG97_DEFAULT_ADDR;
    _addr = addr;

    sendData(CMD_UNCONDITIONAL_STOP_OUTPUT);
    this->change_output_mode(CMD_STOP_OUTPUT);
    // Set initial lastRed time so that we read right away
    _lastRead = 0;
    _lastRead -= UPDATE_TIME_MILLISECOUNDS;

}

/*!
 * PZEM004Tv30::updateValues
 *
 * Read all registers of device and update the local values
 *
 * @return success
*/
bool CG97COULOMETER::updateValues()
{
    //static uint8_t buffer[] = {0x00, CMD_RIR, 0x00, 0x00, 0x00, 0x0A, 0x00, 0x00};
	static uint8_t response[MESSAGE_LENGTH];

    // If we read before the update time limit, do not update
    if(_lastRead + UPDATE_TIME_MILLISECOUNDS > this->millis()){
        return true;
    }

    // Read a Output a set of data
    this->change_output_mode(CMD_OUTPUT_A_SET_OF_DATA);

    if(recieve(response, MESSAGE_LENGTH) != MESSAGE_LENGTH){ // Something went wrong
        return false;
    }
    // Update the current values
    _currentValues.battery_percentage = response[5]; //Raw  battery percentage

    _currentValues.temperature = ((uint32_t)response[6] << 8 |
                              	  (uint32_t)response[7]) / 10.0; // Raw temperature in ceusius

    _currentValues.voltage =  (
								(
									(uint32_t)response[8] << 8 |
									(uint32_t)response[9]
							    ) |
								(
									(uint32_t)response[10] << 8 |
									(uint32_t)response[11]
								)
							  ) * 0.01; // Raw voltage in 0.01V

    float current_multiplier = 0.01;
    if (response[16])
    {
    	current_multiplier *= -1;
    }

    _currentValues.current =  (
								(
									(uint32_t)response[12] << 8 |
									(uint32_t)response[13]
							    ) |
								(
									(uint32_t)response[14] << 8 |
									(uint32_t)response[15]
							    )
							  ) * current_multiplier;  // Raw current in 10mA

    _currentValues.eletrical_value = (
										(
											(uint32_t)response[17] << 8 |
											(uint32_t)response[18]
										) |
										(
											(uint32_t)response[19] << 8 |
											(uint32_t)response[20]
									    )
									 ) * 0.001;  // Raw eletrical_value in 1mAH

    _currentValues.read_time =      (
										(
											(uint32_t)response[21] << 8 |
											(uint32_t)response[22]
										) |
										(
											(uint32_t)response[23] << 8 |
											(uint32_t)response[24]
										)
									 );// Raw read_time in 1 minute


    float power_divider = 1 * pow(10, response[29]) ;

    _currentValues.power =   (
								(
									(uint32_t)response[25] << 8 |
									(uint32_t)response[26]
								) |
								(
									(uint32_t)response[27] << 8 |
									(uint32_t)response[28]
								)
							 ) / power_divider;// Raw power in 1w

    // Record current time as _lastRead
    _lastRead = millis();


    return true;
}

/*!
 * CG97COULOMETER::issue_zero_clearing_power
 *
 * To issue zero-clearing power instructions to equipment
 *
 * @return temperature on C
*/
// 1.
// Practical example:     77 33 C1 03
// Number correspondence: 01 02 03 04
// 01 02: Identification Header 03: Device Address + 0xC4: This Record Zero Clearing Instruction Code 0x03
void CG97COULOMETER::issue_zero_clearing_power()
{
	this->sendData(CMD_ISSUE_ZERO_CLEANING_POWER);
}

// 2. Send instructions to the device to change the data output mode
// Practical example:     77 33 C0 41
// Number correspondence: 01 02 03 04
// 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [40 - Stop Output 41 - Output a set of data 42 - Start Continuous Output Data] Outgoing by default 40
void CG97COULOMETER::change_output_mode(uint8_t mode)
{
	this->sendData(mode);
}

// 3. Issue instructions to fix the address of the equipment:
// Practical example:     77 33 C0 81
// Number correspondence: 01 02 03 04
// 01 02: Identification Header 03: Device Address + 0xC4: Instruction Code [New Address = 0x81-0x80] Address Range 0x00-0x7f Total 128 So Instruction Code Range is 0x80-0xff
void CG97COULOMETER::fix_address(uint8_t newAddress)
{
	this->sendData(newAddress);
	this->_addr = newAddress;

}

// 4. To issue mandatory instructions to the equipment:
// Practical example:     77 33 8A
// Number correspondence: 01 02 03
// 01 02: Identification Header 03: Instruction Code [8A-Unconditional Stop Output 8B-Unconditional Single Output Set of Data
// This command ignores the device address and can be used to view the device address when it is forgotten that the device address cannot be controlled.
void CG97COULOMETER::set_mandatoty_instructions(uint8_t instructionCode)
{
	this->sendData(instructionCode);

}


/*!
 * CG97COULOMETER::sendData
 *
 * Prepares the 8 byte command buffer and sends
 *
 * @param[in] cmd - Command to send (position 1)
 *
 * @return success
*/
int CG97COULOMETER::sendData(uint8_t cmd)
{
	uint8_t* instruction = (uint8_t*) malloc(4);
	//Set cmd header
	instruction[0] = CMD_IDENTIFICATION_HEADER_00;
	instruction[1] = CMD_IDENTIFICATION_HEADER_01;
	size_t size = 4;

	if (cmd == CMD_UNCONDITIONAL_STOP_OUTPUT || cmd == CMD_UNCONDITIONAL_SINGLE_OUTPUT_SET_OF_DATA)
	{
		size = 3;
		instruction[2] = cmd;
		instruction[3] = 0;
	}
	else
	{
		instruction[2] = this->_addr;
		instruction[3] = cmd;
	}

    //const int len = strlen(data);
    const int txBytes = uart_write_bytes(this->_uart_data->uart_port, instruction, size);
    //ESP_LOGI(TAG, "Wrote %d bytes", txBytes);
    return txBytes;
}




/*!
 * CG97COULOMETER::recieve
 *
 * Receive data from serial with buffer limit and timeout
 *
 * @param[out] resp Memory buffer to hold response. Must be at least `len` long
 * @param[in] len Max number of bytes to read
 *
 * @return number of bytes read
*/
uint16_t CG97COULOMETER::recieve(uint8_t *resp, uint16_t len)
{

    //unsigned long startTime = this->millis(); // Start time for Timeout
    uint8_t index = 0; // Bytes we have read
    //while((index < len) && (this->millis() - startTime < READ_TIMEOUT))
    while(index < len)
    {
    	uint8_t* c = (uint8_t*) malloc(1);
    	int length = uart_read_bytes(this->_uart_data->uart_port, c, 1, READ_TIMEOUT / portTICK_RATE_MS);

		if (length > 0)
		{
			resp[index++] = *c;
		}
		taskYIELD();	// do background netw tasks while blocked for IO (prevents ESP watchdog trigger)
    }


    // Check CRC with the number of bytes read
    if(!checkCRC(resp, index)){
    	ESP_LOGI(TAG, "invalid response");
    	return 0;
    }

	ESP_LOGI(TAG, "Receiver %d bytes", index);
	ESP_LOG_BUFFER_HEXDUMP(TAG, resp, index, ESP_LOG_INFO);
    return index;
}

/*!
 * CG97COULOMETER::checkCRC
 *
 * Performs CRC check of the buffer All bytes before the check (last byte) value are added
 *
 * @param[in] data Memory buffer containing the frame to check
 * @param[in] len  Length of the respBuffer
 *
 * @return is the buffer check sum valid
*/
bool CG97COULOMETER::checkCRC(const uint8_t *buf, uint16_t len){
    if(len != MESSAGE_LENGTH) // Sanity check
    {
    	//ESP_LOGI(TAG, "length grater than %i", len);
    	return false;
    }

    uint8_t crc = 0;
    for (uint8_t i = 0; i < len-1; ++i) {
    	crc += buf[i];
    	//ESP_LOGI(TAG, " CRC %i for byte %i", crc, len);
	}

    return buf[len-1] == crc;
}


/*!
 * CG97COULOMETER::millis
 *
 * Get time in millisecounds since boot
 *
 * @return time in millisecounds since boot
*/
uint64_t CG97COULOMETER::millis()
{
	return (uint64_t)(esp_timer_get_time()/1000);
}
