#ifndef TASK_SYNC_H
#define TASK_SYNC_H

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "freertos/event_groups.h"
#include "esp_log.h"
#include "esp_event.h"
#include "mqtt_client.h"
#include "CG97COULOMETER.h"


#include <iostream>
#include <sstream>
#include "sdkconfig.h"
using namespace std;



#define STACK_SIZE 1024 *3

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
static const int WIFI_CONNECTED_BIT = BIT0;
#ifdef CONFIG_USE_WIFI_SMARTCONFIG
static const int ESPTOUCH_DONE_BIT = BIT1;
#endif
static const int MQTT_CONNECTED_BIT = BIT2;
static const int WIFI_CONNECTION_STARTED_BIT = BIT3;
static const int MQTT_CLIENT_CREATED_BIT = BIT4;
static const int MQTT_CLIENT_STARTED_BIT = BIT5;


/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
void task_sync_start();
EventGroupHandle_t get_event_group();
TickType_t get_tick_type_for_milliseconds(uint32_t milliseconds);
bool esp_wait_for_wifi_connection(const TickType_t xTicksToWait = portMAX_DELAY);
bool esp_wait_for_mqtt_connection(const TickType_t xTicksToWait = portMAX_DELAY);
EventBits_t esp_set_bits(const EventBits_t uxBitsToClear);
EventBits_t esp_clear_bits(const EventBits_t uxBitsToClear);

#endif // TASK_SYNC_H
