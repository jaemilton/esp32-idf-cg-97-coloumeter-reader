
#include "tasks_sync.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "cmd_wifi_smartconfig.h"
#include "cmd_mqtt_client.h"
/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t s_event_group;
static const char *TAG_TASK_SYNC = "task_sync";


bool esp_wait_for_bits(const int BITS_TO_CHECK, const TickType_t xTicksToWait);


static void keep_connections(void *arg)
{
	ESP_LOGI(TAG_TASK_SYNC, "keep_connections task starded");
	while(1)
	{
		if (!esp_wait_for_bits(WIFI_CONNECTION_STARTED_BIT, get_tick_type_for_milliseconds(500)))
		{
			wifi_connect();
		}
		else
		{
			if (esp_wait_for_wifi_connection(get_tick_type_for_milliseconds(500)))
			{
				if (!esp_wait_for_bits(MQTT_CLIENT_CREATED_BIT, get_tick_type_for_milliseconds(500)))
				{
					mqtt_client_create();
				}
				else if (!esp_wait_for_bits(MQTT_CLIENT_STARTED_BIT, get_tick_type_for_milliseconds(500)))
				{
					mqtt_client_start();
				}
			}
		}
		vTaskDelay( get_tick_type_for_milliseconds(1000));
		//taskYIELD();	// do background netw tasks while blocked for IO (prevents ESP watchdog trigger)
	}

}

void task_sync_start()
{
	esp_log_level_set(TAG_TASK_SYNC, ESP_LOG_VERBOSE);
	s_event_group = xEventGroupCreate();
	configASSERT( s_event_group );
	ESP_LOGI(TAG_TASK_SYNC, "task_sync started");

	#ifdef CONFIG_USE_WIFI_SMARTCONFIG
	esp_clear_bits(ESPTOUCH_DONE_BIT);
	#endif
	esp_clear_bits(WIFI_CONNECTION_STARTED_BIT);
	esp_clear_bits(MQTT_CONNECTED_BIT);
	esp_clear_bits(MQTT_CLIENT_STARTED_BIT);
	esp_clear_bits(MQTT_CLIENT_CREATED_BIT);


	wifi_start();

	xTaskCreate(keep_connections, "keep_connections_task", STACK_SIZE, NULL, configMAX_PRIORITIES-1, NULL);
}


EventGroupHandle_t get_event_group()
{
	return s_event_group;
}

TickType_t get_tick_type_for_milliseconds(uint32_t milliseconds)
{
	return milliseconds/ portTICK_PERIOD_MS;
}

bool esp_wait_for_wifi_connection(const TickType_t xTicksToWait)
{
#ifdef CONFIG_USE_WIFI_SMARTCONFIG
	return esp_wait_for_bits(WIFI_CONNECTED_BIT | ESPTOUCH_DONE_BIT, xTicksToWait);
#else
	return esp_wait_for_bits(WIFI_CONNECTED_BIT, xTicksToWait);
#endif
}

bool esp_wait_for_mqtt_connection(const TickType_t xTicksToWait)
{
	return esp_wait_for_bits(MQTT_CONNECTED_BIT |  WIFI_CONNECTED_BIT, xTicksToWait);
}


bool esp_wait_for_bits(const int BITS_TO_CHECK, const TickType_t xTicksToWait)
{
		EventBits_t uxBits = xEventGroupWaitBits(s_event_group, BITS_TO_CHECK, pdFALSE, pdFALSE, xTicksToWait);
		if((uxBits & BITS_TO_CHECK) == BITS_TO_CHECK) {
			/* xEventGroupWaitBits() returned because all bits were set. */
			return true;
		}
		return false;
}

EventBits_t esp_set_bits(const EventBits_t uxBitsToClear)
{
	return xEventGroupSetBits(s_event_group, uxBitsToClear);
}

EventBits_t esp_clear_bits(const EventBits_t uxBitsToSet)
{
	return xEventGroupClearBits(s_event_group, uxBitsToSet);
}
