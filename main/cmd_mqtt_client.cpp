#include <stdio.h>
#include "esp_system.h"
#include "sdkconfig.h"
#include "esp_tls.h"
#include "cmd_mqtt_client.h"

static esp_mqtt_client_handle_t s_mqtt_client = NULL;

static const char *TAG_MQTT = "mqtt";

#if CONFIG_BROKER_CERTIFICATE_OVERRIDDEN == 1
static const uint8_t mqtt_eclipse_org_pem_start[]  = "-----BEGIN CERTIFICATE-----\n" CONFIG_BROKER_CERTIFICATE_OVERRIDE "\n-----END CERTIFICATE-----";
#else
extern const uint8_t mqtt_ca_crt_start[]   asm("_binary_mqtt_ca_crt_start");
extern const uint8_t mqtt_client_crt_start[]   asm("_binary_mqtt_client_crt_start");
extern const uint8_t mqtt_client_key_start[]   asm("_binary_mqtt_client_key_start");
#endif
//extern const uint8_t mqtt_eclipse_org_pem_end[]   asm("_binary_mqtt_ca_crt_end");
//extern const uint8_t mqtt_eclipse_org_pem_end[]   asm("_binary_mqtt_client_crt_end");
//extern const uint8_t mqtt_eclipse_org_pem_end[]   asm("_binary_mqtt_client_key_end");


int mqtt_send_message(const char* topicName, const char* message)
{
	int msg_id = 0;
	if(esp_wait_for_mqtt_connection()) {
		//ESP_LOGI(TAG_MQTT, "%s >> %s", topicName, message);
		msg_id = esp_mqtt_client_publish(s_mqtt_client, topicName, message, strlen(message), 0, 0);
		//ESP_LOGI(TAG_MQTT, "binary sent with msg_id=%d msg_length %i", msg_id, strlen(message));
	}
    return msg_id;
}


static void mqtt_destroy_client(esp_mqtt_client_handle_t client)
{
	esp_clear_bits(MQTT_CLIENT_CREATED_BIT);
	ESP_ERROR_CHECK(esp_mqtt_client_destroy(client));
}



static esp_err_t mqtt_event_handler_cb(void* args, esp_mqtt_event_handle_t event)
{
	esp_mqtt_client_handle_t client = event->client;
    //int msg_id;
    // your_context_t *context = event->context;
    switch (event->event_id) {
        case MQTT_EVENT_CONNECTED:
        	esp_set_bits(MQTT_CONNECTED_BIT);
        	ESP_LOGI(TAG_MQTT, "MQTT_EVENT_CONNECTED");
            //msg_id = esp_mqtt_client_subscribe(client, "/topic/delaytime", 0);
            //ESP_LOGI(TAG_MQTT, "sent subscribe successful, msg_id=%d", msg_id);

            	break;
        case MQTT_EVENT_DISCONNECTED:
        	//Try to reconnect on case of disconnection
        	esp_clear_bits(MQTT_CONNECTED_BIT | MQTT_CLIENT_STARTED_BIT);
        	ESP_LOGI(TAG_MQTT, "MQTT_EVENT_DISCONNECTED");
        	mqtt_destroy_client(client);

        	break;

//        case MQTT_EVENT_SUBSCRIBED:
//            ESP_LOGI(TAG_MQTT, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
//
//            break;
//        case MQTT_EVENT_UNSUBSCRIBED:
//            ESP_LOGI(TAG_MQTT, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
//            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG_MQTT, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
            break;
//        case MQTT_EVENT_DATA:
//            ESP_LOGI(TAG_MQTT, "MQTT_EVENT_DATA");
//            printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
//            printf("DATA=%.*s\r\n", event->data_len, event->data);
////            if (strncmp(event->topic, "/topic/delaytime", event->topic_len) == 0) {
////				ESP_LOGI(TAG_MQTT, "Updating delaytime");
////				set_pzem004T_update_delay(static_cast<uint32_t>(std::stoul(event->data)));
////			}
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG_MQTT, "MQTT_EVENT_ERROR");
            if (event->error_handle->error_type == MQTT_ERROR_TYPE_ESP_TLS) {
                ESP_LOGI(TAG_MQTT, "Last error code reported from esp-tls: 0x%x", event->error_handle->esp_tls_last_esp_err);
                ESP_LOGI(TAG_MQTT, "Last tls stack error number: 0x%x", event->error_handle->esp_tls_stack_err);
            } else if (event->error_handle->error_type == MQTT_ERROR_TYPE_CONNECTION_REFUSED) {
                ESP_LOGI(TAG_MQTT, "Connection refused error: 0x%x", event->error_handle->connect_return_code);
            } else {
                ESP_LOGW(TAG_MQTT, "Unknown error type: 0x%x", event->error_handle->error_type);
            }
            break;
//        case MQTT_EVENT_BEFORE_CONNECT:
//        	ESP_LOGI(TAG_MQTT, "MQTT_EVENT_BEFORE_CONNECT");
//			break;
        default:
            ESP_LOGI(TAG_MQTT, "Other event id:%d", event->event_id);
            break;
    }
    return ESP_OK;
}

static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
    ESP_LOGD(TAG_MQTT, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
    mqtt_event_handler_cb(handler_args, (esp_mqtt_event_handle_t)event_data);
}


void mqtt_client_create()
{


	esp_log_level_set(TAG_MQTT, ESP_LOG_VERBOSE);
//    esp_log_level_set("esp-tls", ESP_LOG_NONE);
//    esp_log_level_set("MQTT_CLIENT", ESP_LOG_NONE);
//    esp_log_level_set("TRANSPORT_TCP", ESP_LOG_NONE);
//    esp_log_level_set("TRANSPORT_SSL", ESP_LOG_NONE);
//    esp_log_level_set("TRANSPORT", ESP_LOG_NONE);
//    esp_log_level_set("OUTBOX", ESP_LOG_NONE);



	const esp_mqtt_client_config_t mqtt_cfg = {
				.uri = CONFIG_BROKER_URI,
				.client_id = CONFIG_BROKER_CLIENT_ID,
				.cert_pem = (const char *)mqtt_ca_crt_start,
				.client_cert_pem = (const char *)mqtt_client_crt_start,
				.client_key_pem = (const char *)mqtt_client_key_start,
				.transport = MQTT_TRANSPORT_OVER_SSL,
			};

	ESP_LOGI(TAG_MQTT, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
	s_mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
	ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_ERROR, mqtt_event_handler, NULL));
	ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_CONNECTED, mqtt_event_handler, NULL));
	ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_DISCONNECTED, mqtt_event_handler, NULL));
	//ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_SUBSCRIBED, mqtt_event_handler, NULL));
	//ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_UNSUBSCRIBED, mqtt_event_handler, NULL));
	ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_PUBLISHED, mqtt_event_handler, NULL));
	ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_DATA, mqtt_event_handler, NULL));
	//ESP_ERROR_CHECK(esp_mqtt_client_register_event(s_mqtt_client, MQTT_EVENT_BEFORE_CONNECT, mqtt_event_handler, NULL));
	//mqtt_client_start();
	ESP_LOGI(TAG_MQTT, "mqtt client configured. wainting for connection");

	esp_set_bits(MQTT_CLIENT_CREATED_BIT);

}

void mqtt_client_start()
{

	ESP_LOGI(TAG_MQTT, "starting mqtt connection");
	esp_set_bits(MQTT_CLIENT_STARTED_BIT);
	ESP_ERROR_CHECK(esp_mqtt_client_start(s_mqtt_client));

}

