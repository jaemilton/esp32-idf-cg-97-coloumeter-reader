set(PROJECT_NAME "cg97_coulometer_reader")

idf_component_register(SRCS 
							"main.cpp" 
							"include/CG97COULOMETER.cpp"
							"cmd_cg97_coulometer.cpp"
							"tasks_sync.cpp" 
							"cmd_mqtt_client.cpp" 
							"cmd_wifi_smartconfig.cpp"
						INCLUDE_DIRS  "." include
                        PRIV_REQUIRES 
                        	esp_netif 
                        	nvs_flash 
                        	mqtt)
